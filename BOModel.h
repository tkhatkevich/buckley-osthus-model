#pragma once;

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <random>

using std::vector;

enum EOutputFormat {
    EdgeList = 0,
    Map      = 1
};

inline void AddValueToMap(std::unordered_map<size_t, size_t>& map, size_t key, size_t value) {
    if (map.find(key) == map.end()) {
        map[key] = value;
    } else {
        map[key] += value;
    }
}

class TBOModel {
public:
    typedef std::unordered_map< size_t, std::unordered_map<size_t, size_t> > TGraph;

private:
    struct TInterval {
        size_t VertexNo;
        double RightBound;

        TInterval(size_t vertexNo = 0, double rightBound = 0)
            : VertexNo(vertexNo)
            , RightBound(rightBound)
        {}
    };

    static friend bool operator < (const TInterval& left, const TInterval& right) {
        return left.RightBound < right.RightBound;
    }

    static friend bool operator < (double left, const TInterval& right) {
        return left < right.RightBound;
    }

    void Collapse(size_t newSize);

    void RemapGraph();

public:
    TBOModel(double initAttr = 1,
             size_t outDegree = 1)
        : InitAttr(initAttr)
        , OutDegree(outDegree)
        , Graph(1)
        , Intervals(1, TInterval(0, InitAttr + 1))
        , CurrentRBound(InitAttr + 1)
        , NStartFrom(0)
    {
        Graph[0][0] = 2;
        Intervals.reserve(MaxVertices);
    }

    void Generate(size_t nSteps);

    // if starting from dump then process with m=1 continues (file must be in Map format!!)
    void StartFromDump(const char* fileName);

    void DumpToFile(const char* fileName, EOutputFormat format = EdgeList) const;

    void SetInitAttr(double initAttr);

    double GetInitAttr() const;

    void SetOutDegree(size_t outDegree);

    size_t GetOutDegree() const;

    void AttackNetwork(size_t nodesToAttack, bool withRemap = false);

    void NetworkFailure(size_t nodesToFail, bool withRemap = false);

private:
    double InitAttr;
    size_t OutDegree;
    
    TGraph Graph;
    vector<TInterval> Intervals;
    double CurrentRBound;
    size_t NStartFrom;
    std::random_device RandomGenerator;

    static const size_t MaxVertices = 10 * 1000 * 1000;
};