#include "BOModel.h"

int main(int argc, const char* argv[]) {
    TBOModel model(1, 3);
    model.Generate(500);
    model.AttackNetwork(1, true);
    model.DumpToFile("test.out");
    return 0;
}
