#include "BOModel.h"
#include <algorithm>
#include <assert.h>

void TBOModel::SetInitAttr(double initAttr) {
    InitAttr = initAttr;
}

double TBOModel::GetInitAttr() const {
    return InitAttr;
}

void TBOModel::SetOutDegree(size_t outDegree) {
    OutDegree = outDegree;
}

size_t TBOModel::GetOutDegree() const {
    return OutDegree;
}

void TBOModel::DumpToFile(const char* fileName, EOutputFormat format) const {
    std::ofstream cout(fileName);

    if (format == Map) {
        cout << Graph.size() << std::endl;
    }

    for (size_t vertexNo = 0; vertexNo < Graph.size(); ++vertexNo) {
        vector<size_t> links;
        for (auto it = Graph.at(vertexNo).begin(); it != Graph.at(vertexNo).end(); ++it) {
            //change this parameter to control multiedges and loops in output
            size_t linkPower = (it->first == vertexNo) ? it->second / 2 : it->second;
            
            for (size_t n = 0; n < linkPower; ++n) {
                links.push_back(it->first);
            }
        }
        
        if (format == Map) {
            cout << links.size() << " ";
        }
        for (auto it = links.begin(); it != links.end(); ++it) {
            if (format == Map) {
                cout << *it << " ";
            }
            else if (format == EdgeList) {
                if (*it > vertexNo) {
                    continue;
                }
                cout << vertexNo << " " << *it << std::endl;
            }
        }
        if (format == Map) {
            cout << std::endl;
        }
    }
    
    cout.close();
}

void TBOModel::StartFromDump(const char* fileName) {
    std::ifstream cin(fileName);
    
    size_t nVertices = 0;
    cin >> nVertices;
    CurrentRBound = 0;

    for(size_t vertexNo = 0; vertexNo < nVertices; ++vertexNo) {
        size_t nLinks = 0;
        cin >> nLinks;

        for (size_t linkNo = 0; linkNo < nLinks; ++linkNo) {
            size_t link = 0;
            cin >> link;
            AddValueToMap(Graph[vertexNo], link, 1);
        }

        CurrentRBound += nLinks + InitAttr - OutDegree;
        Intervals.push_back(TInterval(vertexNo, CurrentRBound));
    }
    
    cin.close();
    NStartFrom = Graph.size();
}

void TBOModel::Collapse(size_t newSize) {
    if (OutDegree == 1) {
        return;
    }

    TGraph newGraph;
    vector<TInterval> newIntervals(NStartFrom + newSize);
    double newRBound = 0;
    // coping ready model
    for (size_t vertexNo = 0; vertexNo < NStartFrom; ++vertexNo) {
        newGraph[vertexNo] = std::unordered_map<size_t, size_t>();
        size_t nLinks = 0;
        for (auto linkIt = Graph[vertexNo].begin(); linkIt != Graph[vertexNo].end(); ++linkIt) {
            size_t linkNo = linkIt->first > NStartFrom ? (linkIt->first - NStartFrom) / OutDegree + NStartFrom
                                                       : linkIt->first;
            AddValueToMap(newGraph[vertexNo], linkNo, linkIt->second);
            nLinks += linkIt->second;
        }

        newRBound += nLinks + InitAttr - OutDegree;
        newIntervals[vertexNo] = TInterval(vertexNo, newRBound);
    }

    //Collapsing
    for (size_t vertexNo = NStartFrom; vertexNo < (Graph.size()-NStartFrom) / OutDegree; ++vertexNo) {
        newGraph[vertexNo] = std::unordered_map<size_t, size_t>();
        
        size_t beginVertex = NStartFrom + (vertexNo - NStartFrom)*OutDegree;
        for (size_t oldVertex = beginVertex; oldVertex < beginVertex + OutDegree; ++oldVertex) {
            size_t nLinks = 0;
            for (auto linkIt = Graph[oldVertex].begin(); linkIt != Graph[oldVertex].end(); ++linkIt) {
                size_t linkNo = linkIt->first > NStartFrom ? (linkIt->first - NStartFrom) / OutDegree + NStartFrom
                                                           : linkIt->first;
                AddValueToMap(newGraph[vertexNo], linkNo, linkIt->second);
                nLinks += linkIt->second;
            }
            newRBound += nLinks;
        }
        
        newRBound += InitAttr - OutDegree;
        newIntervals[vertexNo] = TInterval(vertexNo, newRBound);
    }
    Graph = newGraph;
    Intervals = newIntervals;
}

void TBOModel::RemapGraph() {
    std::unordered_map<size_t, size_t> indexMap;
    size_t newIndex = 0;
    for (auto it = Graph.begin(); it != Graph.end(); ++it) {
        indexMap[it->first] = newIndex++;    
    }

    TGraph newGraph;
    vector<TInterval> newIntervals(Graph.size());

    double newRBound = 0;
    size_t intervalNo = 0;
    for (auto it = Graph.begin(); it != Graph.end(); ++it) {
        size_t newVertexNo = indexMap[it->first];
        newGraph[newVertexNo] = std::unordered_map<size_t, size_t>();
        size_t nLinks = 0;

        for (auto linkIt = it->second.begin(); linkIt != it->second.end(); ++linkIt) {
            size_t newLinkNo = indexMap[linkIt->first];
            newGraph[newVertexNo][newLinkNo] = linkIt->second;
            nLinks += linkIt->second;
        }

        newRBound += std::max<double>(nLinks + InitAttr - OutDegree, InitAttr);
        newIntervals[intervalNo++] = TInterval(newVertexNo, newRBound);
    }

    Intervals = newIntervals;
    Graph = newGraph;
}

void TBOModel::Generate(size_t nSteps) {
    size_t stepNo = Graph.size();
    const size_t endStep = NStartFrom + OutDegree*nSteps;

    for (; stepNo < endStep; ++stepNo) {
        Graph[stepNo] = std::unordered_map<size_t, size_t>();
        CurrentRBound += InitAttr;
        Intervals.push_back(TInterval(stepNo, CurrentRBound));

        const double experiment = ((double) RandomGenerator() / RandomGenerator.max()) * CurrentRBound;
        vector<TInterval>::const_iterator bucketIter = std::upper_bound(Intervals.begin(), Intervals.end(), experiment); 
        const size_t linkTo = bucketIter->VertexNo;

        AddValueToMap(Graph[stepNo], linkTo, 1);
        AddValueToMap(Graph[linkTo], stepNo, 1);
        
        CurrentRBound += 1;
        Intervals.push_back(TInterval(linkTo, CurrentRBound));
    }

    Collapse(nSteps);
    NStartFrom = Graph.size();
}

void TBOModel::AttackNetwork(size_t nodesToAttack, bool withRemap) {
    for (size_t node = 0; node < nodesToAttack; ++node) {
        Graph.erase(node);
    }
    for (auto it = Graph.begin(); it != Graph.end(); ++it) {
        for (size_t link = 0; link < nodesToAttack; ++link) {
            it->second.erase(link);
        }
    }
        
    if (withRemap) {
        RemapGraph();
        NStartFrom = Graph.size();
    }
}

void TBOModel::NetworkFailure(size_t nodesToFail, bool withRemap) {
    vector<size_t> failedNodes(nodesToFail);
    for (size_t i = 0; i < failedNodes.size(); ++i) {
        failedNodes[i] = RandomGenerator() % Graph.size();
    }

    for (auto nodeIt = failedNodes.begin(); nodeIt != failedNodes.end(); ++nodeIt) {
        Graph.erase(*nodeIt);
    }
    for (auto it = Graph.begin(); it != Graph.end(); ++it) {
        for (auto linkIt = failedNodes.begin(); linkIt != failedNodes.end(); ++linkIt) {
            it->second.erase(*linkIt);
        }
    }

    if (withRemap) {
        RemapGraph();
        NStartFrom = Graph.size();
    }
}